/*
 * RecursiveDispatch.h
 * test_recursive_dispatch
 *
 * Created by François Lamboley on 09/11/2018.
 * Copyright © 2018 happn. All rights reserved.
 */

#ifndef RecursiveDispatch_h
# define RecursiveDispatch_h

# import <dispatch/dispatch.h>

#ifdef __cplusplus
extern "C"
#endif
void dispatch_recursive_sync(dispatch_queue_t queue, dispatch_block_t block);

#endif /* RecursiveDispatch_h */
